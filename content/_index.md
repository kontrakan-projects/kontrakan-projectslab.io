---
title: 'Home'
intro_image: images/undraw_problem_solving_ft81.svg
intro_image_absolute: true # edit /assets/scss/components/_intro-image.scss for full control
intro_image_hide_on_mobile: true
---

# Hello, We Are Kontrakan Projects.

## Builds better businesses by creating joyful digital ideas, products and experiences.
